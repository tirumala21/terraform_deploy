FROM hashicorp/terraform

WORKDIR terraformfiles/
COPY *.tf terraformfiles/
COPY *.txt terraformfiles/

#RUN terraform init
#RUN terraform plan -inupt=false -var 'bucketname=terraform.bucket.1.04.2022'
#RUN terraform apply -var 'bucketname=terraform.bucket.1.04.2022'

CMD ["/bin/bash","ls terraformfiles/"]


